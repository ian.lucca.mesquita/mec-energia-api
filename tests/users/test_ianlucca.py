import json
from datetime import date
import pytest
from rest_framework import status
from rest_framework.test import APIClient
from unittest.mock import patch
from universities.models import University, ConsumerUnit
from contracts.models import Contract, EnergyBill
from tariffs.models import Distributor, Tariff
from users.models import CustomUser, UniversityUser
from utils.user.user_type_util import UserType


@pytest.fixture
def custom_user_instance():
    return CustomUser()

@pytest.fixture
def university_user_instance():
    return UniversityUser()

# Casos de teste para UserType.is_valid_user_type
def test_valid_user_type():
    user_type = 'super_user'
    result = UserType.is_valid_user_type(user_type)
    assert result == user_type

def test_invalid_user_type():
    with pytest.raises(Exception, match=r'Tipo de usuário \(invalid_user_type\) não existe'):
        UserType.is_valid_user_type('invalid_user_type')

def test_invalid_university_user_type_for_university_user_model():
    with pytest.raises(Exception, match=r'Tipo de usuário errado \(super_user\) para este Modelo de Usuário'):
        UserType.is_valid_user_type('super_user', user_model=UniversityUser)


# Casos de teste para UserType.get_user_type_by_model
def test_get_user_type_by_model_super_user():
    user_type = UserType.get_user_type_by_model(CustomUser)
    assert user_type == CustomUser.super_user_type

def test_get_user_type_by_model_university_user():
    user_type = UserType.get_user_type_by_model(UniversityUser)
    assert user_type == CustomUser.university_user_type

# Casos de teste para UserType.get_user_type
def test_get_user_type_custom_user():
    user_type = UserType.get_user_type(CustomUser.university_admin_user_type)
    assert user_type == CustomUser.university_admin_user_type

def test_get_user_type_university_user():
    user_type = UserType.get_user_type(UniversityUser.university_user_type)
    assert user_type == UniversityUser.university_user_type
