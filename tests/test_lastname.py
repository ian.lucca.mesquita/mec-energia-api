import pytest

def lastname(name: str):
    lista_de_nomes = name.split()

    quantidade_de_nomes = len(lista_de_nomes)
    return quantidade_de_nomes

    
  

def test_entrada_vazia():
    assert lastname("") == 0

def test_entrada_nome():
    assert lastname("Joana") == 1

def test_entrada_sobrenome():
    assert lastname("Joana Burgos") == 2
